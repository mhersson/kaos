# Kapacitor Alert Overview System (KAOS)

Extension to the Kapacitor-Alert-Proxy
https://github.com/mhersson/kapacitor-alert-proxy

Consolidate alert statuses from multiple KAP instances into one overview


## Tip
KAOS uses HTTPS
```bash
# Let KAOS bind to low port
setcap CAP_NET_BIND_SERVICE=+ep /path/to/kaos

# Revert changes
setcap -r /path/to/kaos

