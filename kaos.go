//Copyright (c) 2018 Morten Hersson
package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
	"sort"
	"strings"
	"time"
)

// Customer type
type Customer struct {
	Name       string
	ReportTime time.Time
	Alerts     []Alert
	Counters   AlertCounter
}

// Alert type
type Alert struct {
	ID             string
	Duration       int
	Message        string
	Level          string
	PreviousLevel  string
	Time           float64
	Tags           []Tag
	PDIncidentKey  string
	JiraIssue      string
	GrafanaURL     string
	Environment    string
	TimeString     time.Time
	DurationString string
}

// AlertCounter type
type AlertCounter struct {
	Critical int
	Warning  int
	Info     int
}

// Tag type
type Tag struct {
	Key   string
	Value string
}

var customers []Customer                            //nolint: gochecknoglobals
var templates = make(map[string]*template.Template) //nolint: gochecknoglobals

// CustomerBy type
type CustomerBy func(i1, i2 *Customer) bool

// Sort function for Customer type
func (by CustomerBy) Sort(customers []Customer) {
	ps := &customersorter{
		customers: customers,
		by:        by,
	}
	sort.Sort(ps)
}

type customersorter struct {
	customers []Customer
	by        func(i1, i2 *Customer) bool
}

func (s *customersorter) Len() int {
	return len(s.customers)
}

func (s *customersorter) Swap(i, j int) {
	s.customers[i], s.customers[j] = s.customers[j], s.customers[i]
}

func (s *customersorter) Less(i, j int) bool {
	return s.by(&s.customers[i], &s.customers[j])
}

// AlertBy type
type AlertBy func(i1, i2 *Alert) bool

// Sort function for Alert type
func (by AlertBy) Sort(alerts []Alert) {
	ps := &alertSorter{
		alerts: alerts,
		by:     by,
	}
	sort.Sort(ps)
}

type alertSorter struct {
	alerts []Alert
	by     func(i1, i2 *Alert) bool
}

func (s *alertSorter) Len() int {
	return len(s.alerts)
}

func (s *alertSorter) Swap(i, j int) {
	s.alerts[i], s.alerts[j] = s.alerts[j], s.alerts[i]
}

func (s *alertSorter) Less(i, j int) bool {
	return s.by(&s.alerts[i], &s.alerts[j])
}

func (al *Alert) setEnvironment() {
	for _, tag := range al.Tags {
		if tag.Key == "Environment" {
			al.Environment = tag.Value
			break
		}
	}
}

// IsReporting check if customers KAP is reporting
func (c Customer) IsReporting() bool {
	t := time.Now()
	d := t.Sub(c.ReportTime)

	return d.Seconds() < 60
}

func (al Alert) updateCounter(ac *AlertCounter) {
	switch al.Level {
	case "CRITICAL":
		ac.Critical++
	case "WARNING":
		ac.Warning++
	case "INFO":
		ac.Info++
	}
}

func (al *Alert) convertDuration() {
	r := al.Duration % 3600
	h := (al.Duration - r) / 3600
	m := (r - (r % 60)) / 60

	al.DurationString = fmt.Sprintf("%dh%dm%ds", h, m, r%60)
}

func getByNameAndDelete(name string) Customer {
	// This returns a customer if exists and deletes it from the list
	for i, c := range customers {
		if c.Name == name {
			customers = append(customers[:i], customers[i+1:]...)
			return c
		}
	}

	var c Customer

	return c
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "main", customers)
}

func statusHandler(w http.ResponseWriter, r *http.Request) {
	customer := r.URL.Query().Get("customer")
	if len(customer) == 0 {
		http.NotFound(w, r)
		return
	}

	var c []Customer

	for _, v := range customers {
		if v.Name == customer {
			c = append(c, v)
			break
		}
	}

	renderTemplate(w, "status", c)
}

func countersHandler(w http.ResponseWriter, r *http.Request) {
	type MiniAlert struct {
		ID       string
		Duration int
		Level    string
		Message  string
	}

	type LastUpdate struct {
		Counters   AlertCounter
		Alerts     []MiniAlert
		LastUpdate int64
	}

	var cm = make(map[string]LastUpdate)

	for _, v := range customers {
		var alerts []MiniAlert

		for _, a := range v.Alerts {
			alerts = append(alerts, MiniAlert{a.ID, a.Duration, a.Level, a.Message})
		}

		lu := LastUpdate{Counters: v.Counters, Alerts: alerts, LastUpdate: v.ReportTime.Unix()}
		cm[v.Name] = lu
	}

	jsonString, err := json.Marshal(cm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	fmt.Fprintf(w, "%s", jsonString)
}

func updateHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}

	decoder := json.NewDecoder(r.Body)
	j := make(map[string][]Alert)

	err := decoder.Decode(&j)
	if err != nil {
		panic(err)
	}

	rt := func(i1, i2 *Alert) bool {
		return i1.Time > i2.Time
	}

	name := func(i1, i2 *Customer) bool {
		return i1.Name < i2.Name
	}

	for c, al := range j {
		var alerts []Alert

		var counter = AlertCounter{0, 0, 0}

		customer := getByNameAndDelete(strings.ToLower(c))
		customer.Name = strings.ToLower(c)
		customer.ReportTime = time.Now()

		for _, a := range al {
			a.setEnvironment()
			a.updateCounter(&counter)
			a.TimeString = time.Unix(int64(a.Time), 0)
			a.convertDuration()
			alerts = append(alerts, a)
			AlertBy(rt).Sort(alerts)
		}

		customer.Alerts = alerts
		customer.Counters = counter
		customers = append(customers, customer)
		CustomerBy(name).Sort(customers)
	}
}

func renderTemplate(w http.ResponseWriter, tmpl string, data []Customer) {
	err := templates[tmpl].ExecuteTemplate(w, "base", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var validPath = regexp.MustCompile("^/kaos/(main|status|counters|update)/$") //nolint: gochecknoglobals

func makeHandler(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.RemoteAddr, r.Method, r.URL.Path)
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r)
	}
}

func main() {
	templates["status"] = template.Must(template.ParseFiles("templates/status.html", "templates/base.html"))
	templates["main"] = template.Must(template.ParseFiles("templates/main.html", "templates/base.html"))

	http.HandleFunc("/kaos/main/", makeHandler(mainHandler))
	http.HandleFunc("/kaos/status/", makeHandler(statusHandler))
	http.HandleFunc("/kaos/counters/", makeHandler(countersHandler))
	http.HandleFunc("/kaos/update/", makeHandler(updateHandler))

	//fmt.Println(http.ListenAndServeTLS(":443", "server_crt.pem", "server_key.pem", nil))
	fmt.Println(http.ListenAndServe(":8080", nil))
}
